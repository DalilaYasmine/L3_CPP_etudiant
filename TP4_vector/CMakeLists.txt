cmake_minimum_required( VERSION 3.0 )
project( TP3 )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

# programme principal
add_executable( main
	src/main.cpp
	src/FigureGeometrique.hpp
	src/FigureGeometrique.hpp
	src/Ligne.hpp
	src/Ligne.cpp
	src/PolygoneRegulier.cpp
	src/PolygoneRegulier.cpp
)
target_link_libraries( main )

# programme de test
add_executable( main_test
	src/test/main.cpp
	src/FigureGeometrique.hpp
	src/FigureGeometrique.hpp
	src/test/Ligne.cpp
	src/Ligne.hpp
	src/Ligne.cpp
	src/test/PolygoneRegulier.cpp
	src/PolygoneRegulier.cpp
	src/PolygoneRegulier.cpp
)
target_link_libraries( main_test ${PKG_CPPUTEST_LIBRARIES} )
