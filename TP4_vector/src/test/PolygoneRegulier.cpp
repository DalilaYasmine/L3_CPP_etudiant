#include "../PolygoneRegulier.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPolygoneRegulier) {};

TEST(GroupPolygoneRegulier, PolygoneRegulier_Constructeur) {
	try {
		PolygoneRegulier p(Couleur(0, 1, 0), Point(100, 200), 50, 5);
		CHECK(true);
	} catch (...) {
		FAIL("Exception throwed");
	}
}

TEST(GroupPolygoneRegulier, PolygoneRegulier_getPoint) {
	PolygoneRegulier p(Couleur(0, 1, 0), Point(100, 200), 50, 5);

	Point point = p.getPoint(0);
	CHECK_EQUAL(point._x, 150);
	CHECK_EQUAL(point._y, 200);

	point = p.getPoint(3);
	CHECK_EQUAL(point._x, 59);
	CHECK_EQUAL(point._y, 170);
}