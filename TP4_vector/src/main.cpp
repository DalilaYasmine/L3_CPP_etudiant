#include <iostream>
#include <vector>

#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

int main() {
	Ligne l(Couleur(1, 0, 0), Point(0, 0), Point(100, 200));
	l.afficher();

	PolygoneRegulier p(Couleur(0, 1, 0), Point(100, 200), 50, 5);
	p.afficher();

	std::vector<FigureGeometrique*> liste;
	liste.push_back(&l);
	liste.push_back(&p);

	for (FigureGeometrique* f : liste) {
		f->afficher();
	}

	return 0;
}