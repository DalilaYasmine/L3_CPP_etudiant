#pragma once

#ifndef FIGURE_GEOMETRIQUE_HPP
#define FIGURE_GEOMETRIQUE_HPP

#include "Couleur.hpp"

class FigureGeometrique {
protected:
	Couleur _couleur;

public:
	FigureGeometrique(const Couleur& couleur) : _couleur(couleur) {};

	const Couleur& getCouleur() const;

	virtual void afficher() const = 0;
};

#endif // !FIGURE_GEOMETRIQUE_HPP
