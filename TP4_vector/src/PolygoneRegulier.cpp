#include "PolygoneRegulier.hpp"

#define PI 3.14159265
#include <cmath>

#include <iostream>

PolygoneRegulier::PolygoneRegulier(const Couleur& couleur, const Point& centre, int rayon, int nbCotes) : FigureGeometrique(couleur) {
	_nbPoints = nbCotes;

	float delta = 2.f * PI / nbCotes;
	for (int i = 0; i < nbCotes; i++) {
		_points.push_back(Point(
			static_cast<int>(centre._x +  rayon * cos(delta * i)),
			static_cast<int>(centre._y + rayon * sin(delta * i))
		));
	}
}

void PolygoneRegulier::afficher() const {
	std::cout << "PolygoneRegulier "
		<< _couleur._r << "_" << _couleur._g << "_" << _couleur._b;

	for (int i = 0; i < _nbPoints; i++) {
		std::cout << " " << _points[i]._x << "_" << _points[i]._y;
	}

	std::cout << std::endl;
}

int PolygoneRegulier::getNbPoints() const {
	return _nbPoints;
}

const Point& PolygoneRegulier::getPoint(int indice) const {
	if (indice < 0 || indice + 1 >= _nbPoints) {
		throw "Erreur: indice incorrect";
	}

	return _points[indice];
}