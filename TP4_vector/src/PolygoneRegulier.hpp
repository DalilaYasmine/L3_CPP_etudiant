#pragma once

#ifndef POLYGONE_REGULIER_HPP
#define POLYGONE_REGULIER_HPP

#include <vector>

#include "FigureGeometrique.hpp"
#include "Point.hpp"
#include "Couleur.hpp"

class PolygoneRegulier : public FigureGeometrique {
	int _nbPoints;
	std::vector<Point> _points;

public:
	PolygoneRegulier(const Couleur& couleur, const Point& centre, int rayon, int nbCotes);

	void afficher() const;
	int getNbPoints() const;
	const Point& getPoint(int indice) const;
};

#endif // !POLYGONE_REGULIER_HPP
