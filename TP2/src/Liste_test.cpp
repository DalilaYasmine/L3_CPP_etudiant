#include <CppUTest/CommandLineTestRunner.h>
#include "Liste.hpp"

TEST_GROUP(GroupListe) { };

TEST(GroupListe, Liste_test1)  
{
	Liste *liste = new Liste();
    liste->ajouterDevant(5);
    liste->ajouterDevant(7);
    CHECK_EQUAL(liste->getTaille(),2);
    delete liste;
}
TEST(GroupListe, Liste_test2)  
{
	Liste *liste = new Liste();
    liste->ajouterDevant(5);
    liste->ajouterDevant(7);
    CHECK_EQUAL(liste->getElement(1),7);
    delete liste;
}
TEST(GroupListe, Liste_test3)  
{
	Liste *liste = new Liste();
    liste->ajouterDevant(5);
    liste->ajouterDevant(7);
    CHECK_EQUAL(liste->getElement(2),5);
    delete liste;
}

