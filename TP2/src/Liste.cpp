#include "Liste.hpp"
#include <iostream>
Liste::Liste()
{
	_tete = nullptr;
} 
void Liste::ajouterDevant(int valeur)
{
	Noeud *nouv = new Noeud;
	nouv->_valeur = valeur;
	nouv->_suivant = _tete;
	_tete = nouv;
		
}

int Liste::getTaille() const
{
	int i = 0;
	Noeud *compteur = _tete;
	while(compteur != nullptr)
	{
			i+=1;
			compteur = compteur->_suivant;
	}
	return i;

}

int Liste::getElement(int indice) const
{
	Noeud *compteur = _tete;
	int i = 1;
	while(i != indice)
	{
		compteur = compteur->_suivant;
		i+=1;
	}
	return compteur->_valeur;
}

void afficherListe(Liste *liste)
{
	Noeud *n = liste->_tete;
	while(n != nullptr)
	{
		std::cout << n->_valeur << " <- ";
		n = n->_suivant;
	}
	std::cout << std::endl;	
}

Liste::~Liste()
{
	Noeud *n = _tete;
	while(n != nullptr)
	{
		Noeud *compteur = n->_suivant;
		delete n;
		n = compteur;
	}
}
