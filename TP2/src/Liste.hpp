#ifndef _LISTE_HPP_
#define _LISTE_HPP_
struct Noeud
{
int _valeur;
Noeud *_suivant;
};

struct Liste
{
Noeud *_tete;
Liste();
void ajouterDevant(int valeur);
int getElement(int indice) const;
int getTaille() const;
~Liste();
};

void afficherListe(Liste *liste);
#endif
