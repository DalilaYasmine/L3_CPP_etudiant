#include <iostream>
#include "liste.hpp"


Liste::Liste(){
    _tete = nullptr;
}


int Liste::getTaille(){
    int cpt =0;
    Noeud * cour = _tete;

    if(_tete == nullptr){
        return cpt;
    }

    cpt++;

    while(cour->_suivant !=nullptr){
        cpt++;
        cour = cour->_suivant;
    }
    return cpt;
}


void Liste::ajouterDevant(int valeur){
    Noeud * nouveau = new Noeud;
    nouveau ->_valeur = valeur;
    nouveau ->_suivant = _tete;
    _tete = nouveau;
}

int Liste::getElement(int indice){
    Noeud *cour = _tete;

    for(int i=0;i<indice;i++){
        cour = cour->_suivant;
    }
    return cour->_valeur;
}

Liste::~Liste(){
    while(_tete->_suivant !=nullptr){
        Noeud *cour = _tete;
        _tete = _tete->_suivant;
        delete cour;
    }
    delete _tete;
}







