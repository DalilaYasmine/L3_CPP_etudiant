#ifndef LIVRE_CPP
#define LIVRE_CPP
#include "Livre.hpp"
Livre::Livre() :  _titre(""), _auteur(""),_annee(0000)
{}
Livre::Livre(const std::string & titre, const std::string & auteur, int annee) : _titre(titre), _auteur(auteur), _annee(annee)
{
	std::string str(";");
	std::size_t found = _titre.find(str);
	if (found != std::string::npos)
	{
		throw std::string("erreur : titre non valide (';' non autorisé)");
	}
	std::size_t found1 = _auteur.find(str);
	if (found1 != std::string::npos)
	{
		throw std::string("erreur : auteur non valide (';' non autorisé)");
	}		
	std::string str1("\n");
	std::size_t found2 = _titre.find(str1);
	if (found2 != std::string::npos)
	{
		throw std::string("erreur : titre non valide ('\n' non autorisé)");
	}
	std::size_t found3 = _auteur.find(str1);
	if (found3 != std::string::npos)
	{
		throw std::string("erreur : auteur non valide ('\n' non autorisé)");
	}	
}
const std::string & Livre::getTitre() const
{
	return _titre;
}
const std::string & Livre::getAuteur() const
{
	return _auteur;
}
int Livre::getAnnee() const
{
	return _annee;
}
bool Livre::operator <(const Livre & l) const
{
	return (_auteur < l._auteur || (_auteur == l._auteur && _titre < l._titre));
	return false;
}
bool Livre::operator ==(const Livre & l) const
{
	return (_auteur == l._auteur && _titre == l._titre && _annee == l._annee);
	return false;
}
std::ostream & operator <<(std::ostream & os, Livre & l)
{
	os << l.getTitre() + ";" + l.getAuteur() + ";" + std::to_string(l.getAnnee());
	return os;
}
std::istream & operator >>(std::istream & is, Livre & l)
{
	std::string titre;
	std::string auteur;
	std::string annee;
	std::getline(is,titre,';');
	std::getline(is,auteur,';');
	std::getline(is,annee,';');
	int annees = std::stoi(annee);
	l = Livre(titre,auteur,annees);
	return is;
}
#endif
