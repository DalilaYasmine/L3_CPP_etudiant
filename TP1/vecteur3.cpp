#include <stdio.h>
#include <iostream>
#include "vecteur3.h"
#include <string>
#include <sstream>
#include <math.h>
void afficher(vector3D vector)
{
    float X,Y,Z;
    X = vector.x;
    Y = vector.y;
    Z = vector.z;
    std::cout << "( " << X << "," << Y << "," << Z << " )" << std::endl;
}
void vector3D::afficher()
{
    std::cout << "( " << x << "," << y << "," << z << " )" << std::endl;
}
void norme(vector3D vector)
{
    double resultat;
    resultat = sqrt(pow(vector.x,2)+pow(vector.y,2)+pow(vector.z,2));
    std::cout << resultat << std::endl;
}
void produitScal(vector3D vector_1, vector3D vector_2)
{
    double resultat;
    resultat = (vector_1.x*vector_2.x)+(vector_1.y*vector_2.y)+(vector_1.z*vector_2.z);
    std::cout << resultat << std::endl;
}
void addition(vector3D vector_1, vector3D vector_2)
{
	vector3D result;
	result.x = vector_1.x + vector_2.x;
	result.y = vector_1.y + vector_2.y;
	result.z = vector_1.z + vector_2.z;
	result.afficher();
}
