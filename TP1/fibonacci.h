/*#ifndef FIBONACCI_H_INCLUDED

#define FIBONACCI_H_INCLUDED

int fibonaccireccur(int n);
int fibonacciiterative(int n);

#endif // FIBONACCI_H_INCLUDED
*/

int fibonaccireccur(int n) {
  if (n < 2)
    return n;
  else
    return fibonaccireccur(n-1) + fibonaccireccur(n-2);
	
}


double fibonacciiterative(unsigned int n)
{
	int fib[] = {0,1,1};
	for(int i=2; i<=n; i++)
	{
		fib[i%3] = fib[(i-1)%3] + fib[(i-2)%3];
	}
	return fib[n%3];
}

