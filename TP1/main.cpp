#include <stdio.h>
#include <iostream>
#include "Fibonacci.hpp"
#include "vecteur3.h"
int main(int argc, char** argv)
{
	std::cout << fibonacciRecursif(7) << std::endl;
	std::cout << fibonacciIteratif(7) << std::endl;
    std::cout << "Bienvenue dans cette fonction main" << std::endl;
    vector3D vec3D;
    vec3D.x = 2.0;
    vec3D.y = 3.0;
    vec3D.z = 6.0;
    vector3D vec3D1;
    vec3D1.x = 2.0;
    vec3D1.y = 3.0;
    vec3D1.z = 6.0;
    afficher(vec3D);
    vec3D.afficher();
    norme(vec3D);
    produitScal(vec3D,vec3D1);
    addition(vec3D,vec3D1);
	return 0;
}
