#include "../Produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) {};

TEST(GroupProduit, Produit_Constructeur) {
	try {
		Produit p(42, "toto");
		CHECK(true);
	} catch (...) {
		FAIL("Exception throwed");
	}
}

TEST(GroupProduit, Produit_GetId) {
	Produit p(42, "toto");

	CHECK_EQUAL(p.getId(), 42);
}

TEST(GroupProduit, Produit_GetNom) {
	Produit p(42, "toto");

	CHECK_EQUAL(p.getDescription(), "toto");
}