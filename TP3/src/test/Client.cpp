#include "../Client.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) {};

TEST(GroupClient, Client_Constructeur) {
	try {
		Client c(42, "toto");
		CHECK(true);
	} catch (...) {
		FAIL("Exception throwed");
	}
}

TEST(GroupClient, Client_GetId) {
	Client c(42, "toto");

	CHECK_EQUAL(c.getId(), 42);
}

TEST(GroupClient, Client_GetNom) {
	Client c(42, "toto");

	CHECK_EQUAL(c.getNom(), "toto");
}