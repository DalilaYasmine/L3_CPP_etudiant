#include "../Magasin.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) {};

TEST(GroupMagasin, Magasin_Constructeur) {
	try {
		Magasin mag = Magasin();
		CHECK(true);
	} catch (...) {
		FAIL("Exception throwed");
	}
}

// CLIENTS
TEST(GroupMagasin, Magasin_ajouterClient) {
	Magasin mag = Magasin();

	try {
		mag.ajouterClient("test");
		CHECK(true);
	} catch (...) {
		FAIL("Exception throwed");
	}
}

TEST(GroupMagasin, Magasin_nbClients) {
	Magasin mag = Magasin();

	CHECK_EQUAL(mag.nbClients(), 0);

	mag.ajouterClient("test");

	CHECK_EQUAL(mag.nbClients(), 1);
}

TEST(GroupMagasin, Magasin_supprimerClient) {
	Magasin mag = Magasin();

	mag.ajouterClient("test");

	mag.supprimerClient(0);

	CHECK_EQUAL(mag.nbClients(), 0);
}

TEST(GroupMagasin, Magasin_supprimerClientThrow) {
	Magasin mag = Magasin();

	mag.ajouterClient("test");

	CHECK_THROWS( char*, mag.supprimerClient(100));
}


// PRODUITS
TEST(GroupMagasin, Magasin_ajouterProduit) {
	Magasin mag = Magasin();

	try {
		mag.ajouterProduit("test");
		CHECK(true);
	} catch (...) {
		FAIL("Exception throwed");
	}
}

TEST(GroupMagasin, Magasin_nbProduits) {
	Magasin mag = Magasin();

	CHECK_EQUAL(mag.nbProduits(), 0);

	mag.ajouterProduit("test");

	CHECK_EQUAL(mag.nbProduits(), 1);
}

TEST(GroupMagasin, Magasin_supprimerProduit) {
	Magasin mag = Magasin();

	mag.ajouterProduit("test");

	mag.supprimerProduit(0);

	CHECK_EQUAL(mag.nbProduits(), 0);
}

TEST(GroupMagasin, Magasin_supprimerProduitThrow) {
	Magasin mag = Magasin();

	mag.ajouterProduit("test");

	CHECK_THROWS(char*, mag.supprimerProduit(100));
}

// Locations
TEST(GroupMagasin, Magasin_ajouterLocation) {
	Magasin mag = Magasin();

	try {
		mag.ajouterLocation(1,1);
		CHECK(true);
	} catch (...) {
		FAIL("Exception throwed");
	}
}

TEST(GroupMagasin, Magasin_ajouterLocationThrow) {
	Magasin mag = Magasin();

	mag.ajouterLocation(1, 1);

	CHECK_THROWS(char*, mag.ajouterLocation(1, 1));
}


TEST(GroupMagasin, Magasin_nbLocations) {
	Magasin mag = Magasin();

	CHECK_EQUAL(mag.nbLocations(), 0);

	mag.ajouterLocation(1, 1);

	CHECK_EQUAL(mag.nbLocations(), 1);
}

TEST(GroupMagasin, Magasin_supprimerLocation) {
	Magasin mag = Magasin();

	mag.ajouterLocation(1, 1);

	mag.supprimerLocation(1, 1);

	CHECK_EQUAL(mag.nbLocations(), 0);
}

TEST(GroupMagasin, Magasin_supprimerLocationThrow) {
	Magasin mag = Magasin();

	CHECK_THROWS(char*, mag.supprimerLocation(1, 1));
}

TEST(GroupMagasin, Magasin_trouverClientDansLocation) {
	Magasin mag = Magasin();

	mag.ajouterLocation(0, 0);

	CHECK_EQUAL(mag.trouverClientDansLocation(1), false);

	mag.ajouterLocation(1, 1);

	CHECK_EQUAL(mag.trouverClientDansLocation(1), true);

	mag.ajouterLocation(1, 2);

	CHECK_EQUAL(mag.trouverClientDansLocation(1), true);
}

TEST(GroupMagasin, Magasin_calculerClientsLibres) {
	Magasin mag = Magasin();

	mag.ajouterClient("test");

	CHECK_EQUAL(mag.calculerClientsLibres().size(), 1);

	mag.ajouterLocation(0, 0);

	CHECK_EQUAL(mag.calculerClientsLibres().size(), 0);
}

TEST(GroupMagasin, Magasin_trouverProduitDansLocation) {
	Magasin mag = Magasin();

	mag.ajouterLocation(0, 0);

	CHECK_EQUAL(mag.trouverProduitDansLocation(1), false);

	mag.ajouterLocation(1, 1);

	CHECK_EQUAL(mag.trouverProduitDansLocation(1), true);

	mag.ajouterLocation(2, 1);

	CHECK_EQUAL(mag.trouverProduitDansLocation(1), true);
}

TEST(GroupMagasin, Magasin_calculerProduitsLibres) {
	Magasin mag = Magasin();

	mag.ajouterProduit("test");

	CHECK_EQUAL(mag.calculerProduitsLibres().size(), 1);

	mag.ajouterLocation(0, 0);

	CHECK_EQUAL(mag.calculerProduitsLibres().size(), 0);
}