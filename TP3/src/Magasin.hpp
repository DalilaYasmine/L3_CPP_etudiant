#ifndef MAGASIN_HPP
#define MAGASIN_HPP
#include <iostream>
#include <string>
#include <vector>
#include "Produit.hpp"
#include "Client.hpp"
#include "Location.hpp"
class Magasin{
	private:
	std::vector<Client> _client;
	std::vector<Produit> _produit;
	std::vector<Location> _locations;
	int _idCourantClient;
	int _idCourantProduit;
	public:
	Magasin();
	int nbClients() const;
	void ajouterClient(const std::string & nom);
	void afficherClients() const;
	void supprimerClient(int idClient);
	int nbProduits() const;
	void ajouterProduit(const std::string & nom);
	void afficherProduits() const;
	void supprimerProduit(int idClient); 
};
#endif
