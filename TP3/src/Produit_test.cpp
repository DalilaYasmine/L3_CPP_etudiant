#include <CppUTest/CommandLineTestRunner.h>
#include "Produit.hpp"

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, Produit_test1)  
{
	Produit pPro(1,"Star Citizen");
	CHECK_EQUAL( pPro.getId() , 1);
	CHECK_EQUAL( pPro.getDescription() , "Star Citizen");
}
