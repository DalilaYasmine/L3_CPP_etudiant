#include <iostream>
#include <string>
#include <vector>
#include "Produit.hpp"
#include "Client.hpp"
#include "Location.hpp"
#include "Magasin.hpp"
int main(){
		Location pLoc;
		pLoc._idClient = 0;
		pLoc._idProduit = 2;
		pLoc.afficherLocation();
		Client pCli(42,"toto");
		pCli.afficherClient();
		Produit pPro(1,"Star Citizen");
		pPro.afficherProduit();
		Magasin pMag;
		pMag.ajouterClient("Titi");
		pMag.ajouterClient("Toto");
		pMag.ajouterClient("Tata");
		pMag.ajouterClient("Tutu");
		std::cout << "Nombre de clients : " << pMag.nbClients() << std::endl;
		pMag.afficherClients();
		pMag.supprimerClient(0);
		pMag.afficherClients();
		pMag.supprimerClient(2);
		std::cout << "\n\n " << std::endl;
		pMag.supprimerClient(5);
		pMag.afficherClients();
		pMag.ajouterProduit("Salameche");
		pMag.ajouterProduit("Bulbizarre");
		pMag.ajouterProduit("Carapuce");
		std::cout << "Nombre de produits : " << pMag.nbProduits() << std::endl;
		pMag.afficherProduits();
		pMag.supprimerProduit(0);
		pMag.afficherProduits();
		pMag.supprimerProduit(2);
		pMag.afficherProduits();
		std::cout << "\n\n " << std::endl;
		pMag.supprimerProduit(4);
		return 0;
}
