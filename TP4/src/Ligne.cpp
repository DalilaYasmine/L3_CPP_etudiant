#ifndef LIGNE_CPP
#define LIGNE_CPP
#include "Ligne.hpp"
Ligne::Ligne(const Couleur & couleur, const Point & p0, const Point & p1) :
FigureGeometrique(couleur), _p0(p0), _p1(p1)
{}
void Ligne::afficher(const Cairo::RefPtr<Cairo::Context>& context) const{
	//Couleur pCol = _couleur;
	//Point pP0 = _p0;
	//Point pP1 = _p1;
	//std::cout << "Première version :" << std::endl;
	//std::cout << "Ligne " << pCol._r << "_" << pCol._g << "_" << pCol._b << " ";
	//std::cout << pP0._x << "_" << pP0._y << " " << pP1._x << "_" << pP1._y << std::endl;
	std::cout << "Deuxième version :" << std::endl;
	std::cout << "Ligne " << getCouleur()._r << "_" << getCouleur()._g << "_" << getCouleur()._b << " ";
	std::cout << getP0()._x << "_" << getP0()._y << " " << getP1()._x << "_" << getP1()._y << std::endl;
}
const Point & Ligne::getP0() const{
	return _p0;
}
const Point & Ligne::getP1() const{
	return _p1;
}
#endif
