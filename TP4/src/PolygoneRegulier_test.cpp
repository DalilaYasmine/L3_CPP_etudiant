#include <CppUTest/CommandLineTestRunner.h>
#include "PolygoneRegulier.hpp"

TEST_GROUP(GroupPolygoneRegulier) { };

TEST(GroupPolygoneRegulier, PolygoneRegulier_test1)  
{
	Couleur pPen;
	pPen._r = 0.0;
	pPen._g = 1.0;
	pPen._b = 0.0;
	Point pCe;
	pCe._x = 100;
	pCe._y = 200;
	PolygoneRegulier pPenta(pPen,pCe,50,5);
	CHECK_EQUAL( pPenta.getnbPoints() , 5);
	CHECK_EQUAL( pPenta.getPoint(2)._x , 59);
	CHECK_EQUAL( pPenta.getPoint(2)._y , 229);
}
