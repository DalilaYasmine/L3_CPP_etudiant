#include "../Ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) {};

TEST(GroupLigne, Ligne_Constructeur) {
	try {
		Ligne l(Couleur(1, 0, 0), Point(0, 0), Point(100, 200));
		CHECK(true);
	} catch (...) {
		FAIL("Exception throwed");
	}
}

TEST(GroupLigne, Ligne_getPO) {
	Ligne l(Couleur(1, 0, 0), Point(0, 0), Point(100, 200));

	Point p = l.getP0();

	CHECK_EQUAL(p._x, 0);
	CHECK_EQUAL(p._y, 0);
}

TEST(GroupLigne, Ligne_getP1) {
	Ligne l(Couleur(1, 0, 0), Point(0, 0), Point(100, 200));

	Point p = l.getP1();

	CHECK_EQUAL(p._x, 100);
	CHECK_EQUAL(p._y, 200);
}