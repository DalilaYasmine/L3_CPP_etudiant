#include "Image.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Image_test1)  {
	Image img(10,5);
    CHECK_EQUAL(10, img.getLargeur());
}

TEST(GroupImage, Image_test2)  {
	Image img(10,5);
    CHECK_EQUAL(5, img.getHauteur());
}

/*TEST(GroupImage, Image_test3) {
	Image img(10,5);
	img.setPixel(2,3,350);
	CHECK_EQUAL(255, img.getPixel(2,3));
}
TEST(GroupImage, Image_test4)  {
	Image img(10,5);
	img.setPixel(2,3,-4000);
	CHECK_EQUAL(0, img.getPixel(2,3));
}

TEST(GroupImage, Image_test5)  {
	Image img(10,5);
	img.setPixel(2,3,76);
	CHECK_EQUAL(76, img.getPixel(2,3));
}*/

TEST(GroupImage, Image_test6){
	Image img(10,5);
	img.pixel(2,3) = 76;
	CHECK_EQUAL(76,img.pixel(2,3));
}
