#ifndef IMAGE_CPP
#define IMAGE_CPP
#include "image.hpp"
image::image(int largeur, int hauteur){
	_largeur = largeur;
	_hauteur = hauteur;
}

int image::getHauteur() const{
	return _hauteur;
}
int image::getLargeur() const {
	return _largeur;
}
int image::getPixel(int i, int j)const {
	return _pixels(i+(j*_largeur));
}
void image::setPixel(int i, int j,int couleur){
	if(couleur > 255){
		couleur = 255;
		pixels(i+j*_largeur)) = couleur;
	}
	else if(couleur < 0){
		couleur = 0;
		pixels(i+j*_largeur))= couleur;
	}
	else pixels(i+j*_largeur))= couleur;
}


int pixel (int i, int j) const{
}
#endif
