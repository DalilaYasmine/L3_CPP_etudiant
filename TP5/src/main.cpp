#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
int main(int argc, char ** argv){
	Couleur pCol;
	pCol._r = 1.0;
	pCol._g = 0.0;
	pCol._b = 0.0;
	Point pP0;
	pP0._x = 0;
	pP0._y = 0;
	Point pP1;
	pP1._x = 100;
	pP1._y = 200;
	Ligne pLigne(pCol,pP0,pP1);
	//pLigne.afficher();
	Couleur pPen;
	pPen._r = 0.0;
	pPen._g = 1.0;
	pPen._b = 0.0;
	Point pCe;
	pCe._x = 100;
	pCe._y = 200;
	PolygoneRegulier pPenta(pPen,pCe,50,5);
	//pPenta.afficher();
}
