#ifndef POLYGONEREGULIER_CPP
#define POLYGONEREGULIER_CPP
#include "PolygoneRegulier.hpp"
#include <math.h>
	PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes) :
	FigureGeometrique(couleur)
	{
		_nbPoints = nbCotes;
		_points = new Point[nbCotes];
		int idx = 0;
		for(int i = 0; i < 360; i+= 360/nbCotes){
			_points[idx]._x = rayon*cos((float)i*3.14/180.0)+centre._x;
			_points[idx++]._y = rayon*sin((float)i*3.14/180.0)+centre._y;
		}
	}
	void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context>& context) const{
	//std::cout << "PolygoneRegulier " << getCouleur()._r << "_" << getCouleur()._g << "_" << getCouleur()._b << " ";
	context->set_source_rgb(getCouleur()._r,getCouleur()._g,getCouleur()._b);
	context->move_to(getPoint(0)._x,getPoint(0)._y);
		for(int i = 1; i < getnbPoints(); i++){
			context->line_to(getPoint(i)._x,getPoint(i)._y);
			//std::cout << getPoint(i)._x << "_" << getPoint(i)._y << " ";
		}
	//std::cout << std::endl;
	context->stroke();
	}
	int PolygoneRegulier::getnbPoints() const{
		return _nbPoints;
	}
	const Point & PolygoneRegulier::getPoint(int indice) const{
		return _points[indice];
	}
	PolygoneRegulier::~PolygoneRegulier(){
		delete[] _points;
	}
#endif
