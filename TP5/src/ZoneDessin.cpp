#include "ZoneDessin.hpp"
#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
ZoneDessin::ZoneDessin()
{
	Couleur pCol;
	pCol._r = 1.0;
	pCol._g = 0.0;
	pCol._b = 0.0;
	Point pP0;
	pP0._x = 0;
	pP0._y = 0;
	Point pP1;
	pP1._x = 100;
	pP1._y = 200;
	_figures.push_back(new Ligne(pCol,pP0,pP1));
	Couleur pPen;
	pPen._r = 0.0;
	pPen._g = 1.0;
	pPen._b = 0.0;
	Point pCe;
	pCe._x = 100;
	pCe._y = 200;
	_figures.push_back(new PolygoneRegulier(pPen,pCe,50,5));
}

ZoneDessin::~ZoneDessin()
{
	for(int i=0; i< _figures.size(); i++){
		delete _figures[i];
	}
}
	
bool ZoneDessin::on_expose_event(GdkEventExpose* event)
{
	Cairo::RefPtr<Cairo::Context> context = get_window()->create_cairo_context();
	for(int i=0; i< _figures.size(); i++){
		_figures[i]->afficher(context);
	}
}

bool ZoneDessin::gererClic(GdkEventButton* event)
{
	if(event->type == GDK_BUTTON_PRESS && event-> ==3)
	{
		Couleur pPent;
		pPen._r = 0.0;
		pPen._g = 1.0;
		pPen._b = 0.0;
		Point pCentre;
		pCentre._x = (int)event->x;
		pCentre._y = (int)event->y;
		_figures.push_back(new PolygoneRegulier(pPent,pCentre,50,5));
		get_window().invalidate();
		return true;
	}
	if(event->type == GDK_BUTTON_PRESS && event-> ==1)
	{
		delete _figures.back();
		_figures.pop_back();
	}
		return true;
	}
}
