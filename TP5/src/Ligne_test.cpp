#include <CppUTest/CommandLineTestRunner.h>
#include "Ligne.hpp"

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, Ligne_test1)  
{
	Couleur pCol;
	pCol._r = 1.0;
	pCol._g = 0.0;
	pCol._b = 0.0;
	Point pP0;
	pP0._x = 0;
	pP0._y = 0;
	Point pP1;
	pP1._x = 100;
	pP1._y = 200;
	Ligne pLigne(pCol,pP0,pP1);
	CHECK_EQUAL( pLigne.getP0()._x , 0);
	CHECK_EQUAL( pLigne.getP0()._y , 0);
	CHECK_EQUAL( pLigne.getP1()._x , 100);
	CHECK_EQUAL( pLigne.getP1()._y , 200);
}
