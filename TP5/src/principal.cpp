#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"
int main(int argc, char ** argv){
	ViewerFigures viewer(argc,argv);
	ZoneDessin zone;
	viewer.setZone2D(zone);
	viewer.run();
}
