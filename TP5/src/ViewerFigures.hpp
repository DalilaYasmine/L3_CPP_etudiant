#ifndef VIEWERFIGURES_HPP
#define VIEWERFIGURES_HPP
#include <gtkmm.h>
#include "ZoneDessin.hpp"
class ViewerFigures{
	private:
	Gtk::Main _kit;
	Gtk::Window _window;
	public:
	ViewerFigures(int argc, char ** argv) : _kit(argc,argv){
		_window.set_title("Fenêtre Principale");
		_window.set_default_size(640,480);
		}
	void setZone2D(ZoneDessin& zone2D){
		_window.add(zone2D);
	}	
	void run(){
		_window.show_all();
		_kit.run(_window);
	}
};
#endif
